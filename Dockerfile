FROM sutty/sdk:latest
MAINTAINER "f <f@sutty.nl>"

COPY ./build.sh /usr/local/bin/build
RUN chmod 755 /usr/local/bin/build
RUN sed -i -e 's/v[[:digit:]]\..*\//edge\//g' /etc/apk/repositories
RUN echo 'https://alpine.sutty.nl/alpine/v3.12/sutty' >> /etc/apk/repositories
RUN wget https://alpine.sutty.nl/alpine/sutty.pub -O /etc/apk/keys/alpine@sutty.nl-5ea884cd.rsa.pub
RUN apk update || true

USER builder
ENTRYPOINT /usr/local/bin/build
